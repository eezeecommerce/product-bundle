<?php

namespace eezeecommerce\ProductBundle\Form;

use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\UploadBundle\Form\FilesType;

class OptionsType extends AbstractType
{
    /**
     * @var CurrencyItem
     */
    private $currency;

    public function __construct(CurrencyProvider $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currencyCode = $this->currency->loadCurrency()->getEntity()->getCurrencyCode();

        $builder
                ->add('type', 'choice', array(
                    "label" => false,
                    'choices' => array(
                        'file' => 'File Upload',
                        'text' => 'Text Input',
                        'array' => 'Multi select',
                        'textarea' => 'Large Text Box',
                        'other' => 'Other'
                    ),
                ))
                ->add('name', null, array(
                    "label" => false,
                ))
                ->add('description', null, array(
                    "label" => false,
                ))
                ->add('locale', null, array(
                    "label" => false,
                ))
                ->add('optional', null, array(
                    "required" => false,
                    "label" => "Is Optional?"
                ))
                ->add('image', "collection", array(
                    "label" => false,
                    'type' => new FilesType(),
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'prototype_name' => "__productoption_image__",
                ))
                ->add('option_choices', "collection", array(
                    "label" => false,
                    'type' => new OptionChoicesType($this->currency),
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'prototype_name' => "__productoption_option__",
                ))
                ->add('stock_code', null, array(
                    "label" => false,
                ))
                ->add('bay_number', null, array(
                    "label" => false,
                ))
                ->add('max_length', null, array(
                    "label" => false,
                    "required" => false
                ))
                ->add('placeholder', null, array(
                    "label" => false,
                    "required" => false
                ))
                ->add('base_price', 'money', array(
                    "label" => false,
                    "currency" => $currencyCode
                ))
                ->add('slaves', 'entity', array(
                    'class' => 'eezeecommerceProductBundle:Options',
                    'property' => 'name',
                    'multiple' => true,
                    "required" => false,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ProductBundle\Entity\Options'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_productbundle_options';
    }

}
