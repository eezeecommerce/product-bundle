<?php

namespace eezeecommerce\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\StockBundle\Form\StockType;

class VariantsType extends AbstractType
{
    /**
     * @var string
     */
    private $currencyCode;

    public function __construct($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('product_name')
                ->add('sku')
                ->add('stock_code')
                ->add('bay_number')
                ->add('base_price', 'money', array(
                    "currency" => $this->currencyCode
                ))
                ->add('length')
                ->add('height')
                ->add('depth')
                ->add('weight')
                ->add('stock', new StockType())
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ProductBundle\Entity\Variants'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_productbundle_variants';
    }

}
