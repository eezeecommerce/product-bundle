<?php

namespace eezeecommerce\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;

class OptionChoicesType extends AbstractType
{
    /**
     * @var CurrencyItem
     */
    private $currency;

    public function __construct(CurrencyProvider $currency)
    {
        $this->currency = $currency->loadCurrency();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $currencyCode = $this->currency->getEntity()->getCurrencyCode();

        $builder
                ->add('name')
                ->add('base_price', MoneyType::class, [
                    "currency" => $currencyCode
                ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ProductBundle\Entity\OptionChoices'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_productbundle_optionchoices';
    }

}
