<?php

namespace eezeecommerce\ProductBundle\Form;

use eezeecommerce\CurrencyBundle\Currency\CurrencyItem;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use eezeecommerce\CurrencyBundle\Provider\CurrencyProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\UploadBundle\Form\FilesType;
use eezeecommerce\WebBundle\Form\UriType;
use eezeecommerce\StockBundle\Form\StockType;

class ProductType extends AbstractType
{
    /**
     * @var CurrencyItem
     */
    private $currency;

    public function __construct(CurrencyProvider $currency)
    {
        $this->currency = $currency->loadCurrency();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $currencyCode = $this->currency->getEntity()->getCurrencyCode();

        $builder
                ->add('image', "collection", array(
                    "label" => false,
                    'type' => new FilesType(),
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'prototype_name' => "__product_image__",
                ))
            ->add(
                'thumbnail_image',
                FilesType::class,
                array(
                    "label" => false,
                    "required" => false,
                )
            )
                ->add('product_name', null, array(
                    "label" => false,
                ))
                ->add('long_description', null, array(
                    "label" => false,
                ))
                ->add('short_description', null, array(
                    "label" => false,
                ))
                ->add('uri', new UriType())
                ->add('locale', null, array(
                    "label" => false,
                ))
                ->add('sku', null, array(
                    "label" => false,
                ))
                ->add('stock_code', null, array(
                    "label" => false,
                ))
                ->add('bay_number', null, array(
                    "label" => false,
                ))
                ->add("base_price", MoneyType::class, [
                    "label" => false,
                    "currency" => $currencyCode
                ])
                ->add('length', null, array(
                    "label" => false,
                ))
                ->add('height', null, array(
                    "label" => false,
                ))
                ->add('depth', null, array(
                    "label" => false,
                ))
                ->add('weight', null, array(
                    "label" => false,
                ))
                ->add('turnaround_min_override', null, array(
                    "label" => "Minimum Turnaround Time",
                ))
                ->add('turnaround_max_override', null, array(
                    "label" => "Maximum Turnaround Time",
                ))
                ->add('options', 'entity', array(
                    'class' => 'eezeecommerceProductBundle:Options',
                    'property' => 'stockCode',
                    'multiple' => true,
                    "required" => false,
                ))
                ->add('category', 'entity', array(
                    'class' => 'eezeecommerceCategoryBundle:Category',
                    'property' => 'title',
                    'multiple' => true,
                    "required" => false,
                ))
                ->add('variants', "collection", array(
                    "label" => false,
                    'type' => new VariantsType($currencyCode),
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'prototype_name' => "__variant__",
                ))
                ->add('attribute', "collection", array(
                    "label" => false,
                    'type' => new AttributesType(),
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'prototype_name' => "__attributes__",
                ))
                ->add('stock', new StockType())
                ->add('was', null, [
                    "label" => false
                ])
                ->add("was_price", MoneyType::class, [
                    "label" => false,
                    "required" => false,
                    "currency" => $currencyCode
                ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ProductBundle\Entity\Product'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_productbundle_product';
    }

}
