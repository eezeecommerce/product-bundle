<?php
/**
 * Created by PhpStorm.
 * User: liam
 * Date: 22/02/2017
 * Time: 15:43
 */

namespace eezeecommerce\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class AttributesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('value');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\ProductBundle\Entity\Attribute'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_productbundle_attributes';
    }
}