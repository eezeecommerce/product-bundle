<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\ProductBundle\Services;

/**
 * Description of ProductService
 *
 * @author root
 */
class ProductService
{

    private $query;
    private $entityManager;

    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
    }

    /**
     *  Expects a ID of the products. e.g. 1, 121.
     * @param type $product
     */
    public function setProduct($product)
    {
        if (!is_int($product)) {
            throw new \InvalidArgumentException(sprintf("Expected type to be int. Instead got: %s", gettype($product)));
        }

        $this->product = $product;
                
    }

    public function getProduct()
    {
        if (null === $this->product) {
            throw new \InvalidArgumentException("Product data should be set before trying to getProduct Data");
        }
    }

}
