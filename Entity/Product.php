<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use eezeecommerce\CartBundle\Core\EntityInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Translatable\Translatable;
use eezeecommerce\UploadBundle\Entity\Files;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="eezeecommerce\ProductBundle\Entity\ProductRepository")
 * @ORM\Table(name="product", indexes={@ORM\Index(columns={"stock_code", "product_name", "short_description", "long_description"}, flags={"fulltext"})})
 * @UniqueEntity(fields="stock_code", message="A product already exists with this stock code.", ignoreNull=false)
 */
class Product implements Translatable, EntityInterface
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=50)
     * @Gedmo\Translatable
     */
    protected $product_name;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Gedmo\Translatable
     * @ORM\Column(name="long_description", type="text", nullable=true)
     */
    protected $long_description;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(name="short_description", type="string", length=75, nullable=true)
     * @Gedmo\Translatable
     */
    protected $short_description;

    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="\eezeecommerce\WebBundle\Entity\Uri", mappedBy="product", cascade={"persist", "remove"})
     */
    protected $uri;

    /**
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $sku;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $stock_code;

    /**
     * @Assert\Type("int")
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $turnaround_min_override;

    /**
     * @Assert\Type("int")
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $turnaround_max_override;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $bay_number;

    /**
     * @Assert\NotNull
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $base_price;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $length;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $height;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $depth;

    /**
     * @Assert\NotNull
     * @Assert\Type("boolean")
     * @ORM\Column(type="boolean")
     */
    protected $disabled = false;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $weight;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="Variants", mappedBy="product", cascade={"persist", "remove"}, fetch="EAGER")
     * @ORM\OrderBy({"base_price" = "ASC"})
     */
    protected $variants;

    /**
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\ProductBundle\Entity\Options", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="product_options",
     * joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="option_id", referencedColumnName="id")})
     */
    protected $options;

    /**
     * @Assert\Valid
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\CategoryBundle\Entity\Category", inversedBy="product", cascade={"persist"})
     * @ORM\JoinTable(name="product_categories")
     */
    protected $category;

    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="eezeecommerce\StockBundle\Entity\Stock", mappedBy="product", cascade={"persist"})
     */
    protected $stock;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="eezeecommerce\OrderBundle\Entity\OrderLines", mappedBy="product")
     */
    protected $orderlines;

    /**
     * @Assert\Type("boolean")
     * @ORM\Column(type="boolean")
     */
    protected $was = false;

    /**
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=19, scale=4, nullable=true)
     */
    protected $was_price;

    /**
     * @Assert\Valid
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\UploadBundle\Entity\Files", cascade={"persist"})
     * @ORM\JoinTable(name="product_files",
     * joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")})
     */
    private $image;

    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\UploadBundle\Entity\Files", cascade={"persist"})
     * @ORM\JoinColumn(name="thumbnail_image_id", referencedColumnName="id")
     */
    private $thumbnail_image;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="\eezeecommerce\ProductBundle\Entity\Attribute", mappedBy="product", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $attribute;

    /**
     * @var string $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\Column(type="string")
     */
    private $createdBy;

    /**
     * @var string $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\Column(type="string")
     */
    private $updatedBy;

    /**
     * @var string $contentChangedBy
     *
     * @ORM\Column(name="content_changed_by", type="string", nullable=true)
     * @Gedmo\Blameable(on="change", field={"attribute", "image", "product_name", "long_description", "short_description", "sku", "stock_code", "turnaround_min_override", "turnaround_max_override", "bay_number", "base_price", "disabled", "variants", "options", "category", "stock"})
     */
    private $contentChangedBy;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->variants = new ArrayCollection();
        $this->attribute = new ArrayCollection();
        $this->options = new ArrayCollection();
        $this->image = new ArrayCollection();
        $this->orderlines = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->short_description;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get productName
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * Set productName
     *
     * @param string $productName
     *
     * @return Product
     */
    public function setProductName($productName)
    {
        $this->product_name = $productName;

        return $this;
    }

    /**
     * Get longDescription
     *
     * @return string
     */
    public function getLongDescription()
    {
        return $this->long_description;
    }

    /**
     * Set longDescription
     *
     * @param string $longDescription
     *
     * @return Product
     */
    public function setLongDescription($longDescription)
    {
        $this->long_description = $longDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return Product
     */
    public function setShortDescription($shortDescription)
    {
        $this->short_description = $shortDescription;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Product
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get stockCode
     *
     * @return string
     */
    public function getStockCode()
    {
        return $this->stock_code;
    }

    /**
     * Set stockCode
     *
     * @param string $stockCode
     *
     * @return Product
     */
    public function setStockCode($stockCode)
    {
        $this->stock_code = $stockCode;

        return $this;
    }

    /**
     * Get bayNumber
     *
     * @return string
     */
    public function getBayNumber()
    {
        return $this->bay_number;
    }

    /**
     * Set bayNumber
     *
     * @param string $bayNumber
     *
     * @return Product
     */
    public function setBayNumber($bayNumber)
    {
        $this->bay_number = $bayNumber;

        return $this;
    }

    /**
     * Get length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set length
     *
     * @param string $length
     *
     * @return Product
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set height
     *
     * @param string $height
     *
     * @return Product
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get depth
     *
     * @return string
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set depth
     *
     * @param string $depth
     *
     * @return Product
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Product
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Add image
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $image
     *
     * @return Product
     */
    public function addImage(\eezeecommerce\UploadBundle\Entity\Files $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $image
     */
    public function removeImage(\eezeecommerce\UploadBundle\Entity\Files $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get uri
     *
     * @return \eezeecommerce\WebBundle\Entity\Uri
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set uri
     *
     * @param \eezeecommerce\WebBundle\Entity\Uri $uri
     *
     * @return Product
     */
    public function setUri(\eezeecommerce\WebBundle\Entity\Uri $uri = null)
    {
        $this->uri = $uri;
        $uri->setProduct($this);
        $uri->setUrl($this->stock_code);

        return $this;
    }

    /**
     * Add variant
     *
     * @param \eezeecommerce\ProductBundle\Entity\Variants $variant
     *
     * @return Product
     */
    public function addVariant(\eezeecommerce\ProductBundle\Entity\Variants $variant)
    {
        $this->variants[] = $variant;

        $variant->setProduct($this);

        return $this;
    }

    /**
     * Remove variant
     *
     * @param \eezeecommerce\ProductBundle\Entity\Variants $variant
     */
    public function removeVariant(\eezeecommerce\ProductBundle\Entity\Variants $variant)
    {
        $this->variants->removeElement($variant);
    }

    /**
     * Get variants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * Add option
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $option
     *
     * @return Product
     */
    public function addOption(\eezeecommerce\ProductBundle\Entity\Options $option)
    {
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $option
     */
    public function removeOption(\eezeecommerce\ProductBundle\Entity\Options $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Add category
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $category
     *
     * @return Product
     */
    public function addCategory(\eezeecommerce\CategoryBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $category
     */
    public function removeCategory(\eezeecommerce\CategoryBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get basePrice
     *
     * @return string
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * Set basePrice
     *
     * @param string $basePrice
     *
     * @return Product
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \eezeecommerce\StockBundle\Entity\Stock
     */
    public function getStock()
    {
        if (null === $this->stock) {
            $this->setStock(new \eezeecommerce\StockBundle\Entity\Stock());
        }

        return $this->stock;
    }

    /**
     * Set stock
     *
     * @param \eezeecommerce\StockBundle\Entity\Stock $stock
     *
     * @return Product
     */
    public function setStock(\eezeecommerce\StockBundle\Entity\Stock $stock = null)
    {
        $this->stock = $stock;
        $stock->setProduct($this);

        return $this;
    }

    /**
     * Get turnaroundMinOverride
     *
     * @return \DateTime
     */
    public function getTurnaroundMinOverride()
    {
        return $this->turnaround_min_override;
    }

    /**
     * Set turnaroundMinOverride
     *
     * @param \DateTime $turnaroundMinOverride
     *
     * @return Product
     */
    public function setTurnaroundMinOverride($turnaroundMinOverride)
    {
        $this->turnaround_min_override = $turnaroundMinOverride;

        return $this;
    }

    /**
     * Get turnaroundMaxOverride
     *
     * @return \DateTime
     */
    public function getTurnaroundMaxOverride()
    {
        return $this->turnaround_max_override;
    }

    /**
     * Set turnaroundMaxOverride
     *
     * @param \DateTime $turnaroundMaxOverride
     *
     * @return Product
     */
    public function setTurnaroundMaxOverride($turnaroundMaxOverride)
    {
        $this->turnaround_max_override = $turnaroundMaxOverride;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     *
     * @return Product
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Add orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderLines $orderline
     *
     * @return Product
     */
    public function addOrderline(\eezeecommerce\OrderBundle\Entity\OrderLines $orderline)
    {
        $this->orderlines[] = $orderline;

        return $this;
    }

    /**
     * Remove orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderLines $orderline
     */
    public function removeOrderline(\eezeecommerce\OrderBundle\Entity\OrderLines $orderline)
    {
        $this->orderlines->removeElement($orderline);
    }

    /**
     * Get orderlines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderlines()
    {
        return $this->orderlines;
    }

    /**
     * Get was
     *
     * @return boolean
     */
    public function getWas()
    {
        return $this->was;
    }

    /**
     * Set was
     *
     * @param boolean $was
     *
     * @return Product
     */
    public function setWas($was)
    {
        $this->was = $was;

        return $this;
    }

    /**
     * Get wasPrice
     *
     * @return string
     */
    public function getWasPrice()
    {
        return $this->was_price;
    }

    /**
     * Set wasPrice
     *
     * @param string $wasPrice
     *
     * @return Product
     */
    public function setWasPrice($wasPrice)
    {
        $this->was_price = $wasPrice;

        return $this;
    }

    /**
     * Get thumbnailImage
     *
     * @return \eezeecommerce\UploadBundle\Entity\Files
     */
    public function getThumbnailImage()
    {
        return $this->thumbnail_image;
    }

    /**
     * Set thumbnailImage
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $thumbnailImage
     *
     * @return Product
     */
    public function setThumbnailImage(\eezeecommerce\UploadBundle\Entity\Files $thumbnailImage = null)
    {
        $this->thumbnail_image = $thumbnailImage;

        return $this;
    }

    /**
     * Add attribute
     *
     * @param \eezeecommerce\ProductBundle\Entity\Attribute $attribute
     *
     * @return Product
     */
    public function addAttribute(\eezeecommerce\ProductBundle\Entity\Attribute $attribute)
    {
        $this->attribute[] = $attribute;

        $attribute->setProduct($this);

        return $this;
    }

    /**
     * Remove attribute
     *
     * @param \eezeecommerce\ProductBundle\Entity\Attribute $attribute
     */
    public function removeAttribute(\eezeecommerce\ProductBundle\Entity\Attribute $attribute)
    {
        $this->attribute->removeElement($attribute);
    }

    /**
     * Get attribute
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set outOfStock
     *
     * @param boolean $outOfStock
     *
     * @return Product
     */
    public function setOutOfStock($outOfStock)
    {
        $this->out_of_stock = $outOfStock;

        return $this;
    }

    /**
     * Get outOfStock
     *
     * @return boolean
     */
    public function getOutOfStock()
    {
        return $this->out_of_stock;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Product
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     *
     * @return Product
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set contentChangedBy
     *
     * @param string $contentChangedBy
     *
     * @return Product
     */
    public function setContentChangedBy($contentChangedBy)
    {
        $this->contentChangedBy = $contentChangedBy;

        return $this;
    }

    /**
     * Get contentChangedBy
     *
     * @return string
     */
    public function getContentChangedBy()
    {
        return $this->contentChangedBy;
    }
}
