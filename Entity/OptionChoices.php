<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="option_choices")
 * @ORM\Entity(repositoryClass="eezeecommerce\ProductBundle\Entity\OptionChoicesRepository")
 */
class OptionChoices implements Translatable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=50)
     */
    protected $name;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $base_price;
    
    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="eezeecommerce\ProductBundle\Entity\Options", inversedBy="option_choices")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    protected $option;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="eezeecommerce\OrderBundle\Entity\OptionsOrderlines", mappedBy="option_choice")
     */
    protected $optionsorderlines;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->optionsorderlines = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OptionChoices
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get option
     *
     * @return \eezeecommerce\ProductBundle\Entity\Options
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set option
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $option
     *
     * @return OptionChoices
     */
    public function setOption(\eezeecommerce\ProductBundle\Entity\Options $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get basePrice
     *
     * @return string
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * Set basePrice
     *
     * @param string $basePrice
     *
     * @return OptionChoices
     */
    public function setBasePrice($basePrice)
    {
        if (null === $basePrice) {
            $basePrice = "0.00";
        }
        $this->base_price = $basePrice;

        return $this;
    }

    /**
     * Add optionsorderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OptionsOrderlines $optionsorderline
     *
     * @return OptionChoices
     */
    public function addOptionsorderline(\eezeecommerce\OrderBundle\Entity\OptionsOrderlines $optionsorderline)
    {
        $this->optionsorderlines[] = $optionsorderline;

        return $this;
    }

    /**
     * Remove optionsorderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OptionsOrderlines $optionsorderline
     */
    public function removeOptionsorderline(\eezeecommerce\OrderBundle\Entity\OptionsOrderlines $optionsorderline)
    {
        $this->optionsorderlines->removeElement($optionsorderline);
    }

    /**
     * Get optionsorderlines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionsorderlines()
    {
        return $this->optionsorderlines;
    }
}
