<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Translatable\Translatable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="eezeecommerce\ProductBundle\Entity\VariantsRepository")
 * @ORM\Table(name="variants")
 * @UniqueEntity(fields="stock_code", message="A variant already exists with this stock code.", ignoreNull=false)
 */
class Variants implements Translatable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=50)
     * @Gedmo\Translatable
     */
    protected $product_name;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $sku;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $stock_code;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $bay_number;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $base_price;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $length;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $height;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $depth;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @ORM\Column(type="decimal", precision=10, scale=4)
     */
    protected $weight;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="variants")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="\eezeecommerce\OrderBundle\Entity\OrderLines", mappedBy="variant")
     */
    protected $orderlines;
    
    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="eezeecommerce\StockBundle\Entity\Stock", mappedBy="variant", cascade={"persist"})
     */
    protected $stock;



    /**
     * @var string $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\Column(type="string")
     */
    private $createdBy;

    /**
     * @var string $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\Column(type="string")
     */
    private $updatedBy;

    /**
     * @var string $contentChangedBy
     *
     * @ORM\Column(name="content_changed_by", type="string", nullable=true)
     * @Gedmo\Blameable(on="change", field={"product_name", "sku", "stock_code", "bay_number", "base_price", "product", "stock"})
     */
    private $contentChangedBy;

    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get productName
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * Set productName
     *
     * @param string $productName
     *
     * @return Variants
     */
    public function setProductName($productName)
    {
        $this->product_name = $productName;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Variants
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get stockCode
     *
     * @return string
     */
    public function getStockCode()
    {
        return $this->stock_code;
    }

    /**
     * Set stockCode
     *
     * @param string $stockCode
     *
     * @return Variants
     */
    public function setStockCode($stockCode)
    {
        $this->stock_code = $stockCode;

        return $this;
    }

    /**
     * Get bayNumber
     *
     * @return string
     */
    public function getBayNumber()
    {
        return $this->bay_number;
    }

    /**
     * Set bayNumber
     *
     * @param string $bayNumber
     *
     * @return Variants
     */
    public function setBayNumber($bayNumber)
    {
        $this->bay_number = $bayNumber;

        return $this;
    }

    /**
     * Get length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set length
     *
     * @param string $length
     *
     * @return Variants
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set height
     *
     * @param string $height
     *
     * @return Variants
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get depth
     *
     * @return string
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set depth
     *
     * @param string $depth
     *
     * @return Variants
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Variants
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get product
     *
     * @return \eezeecommerce\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     *
     * @return Variants
     */
    public function setProduct(\eezeecommerce\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get stock
     *
     * @return \eezeecommerce\StockBundle\Entity\Stock
     */
    public function getStock()
    {
        if (null === $this->stock) {
            $this->setStock(new \eezeecommerce\StockBundle\Entity\Stock());
        }

        return $this->stock;
    }

    /**
     * Set stock
     *
     * @param \eezeecommerce\StockBundle\Entity\Stock $stock
     *
     * @return Variants
     */
    public function setStock(\eezeecommerce\StockBundle\Entity\Stock $stock = null)
    {
        $this->stock = $stock;
        $stock->setVariant($this);

        return $this;
    }

    public function getFormProductName()
    {
        if ($this->base_price > 0) {
            return $this->product_name . " - £" . round($this->getBasePrice(), 2);
        }

        return $this->product_name;
    }

    /**
     * Get basePrice
     *
     * @return string
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * Set basePrice
     *
     * @param string $basePrice
     *
     * @return Variants
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;

        return $this;
    }

    /**
     * Add orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderLines $orderline
     *
     * @return Variants
     */
    public function addOrderline(\eezeecommerce\OrderBundle\Entity\OrderLines $orderline)
    {
        $this->orderlines[] = $orderline;

        return $this;
    }

    /**
     * Remove orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OrderLines $orderline
     */
    public function removeOrderline(\eezeecommerce\OrderBundle\Entity\OrderLines $orderline)
    {
        $this->orderlines->removeElement($orderline);
    }

    /**
     * Get orderlines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderlines()
    {
        return $this->orderlines;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Variants
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     *
     * @return Variants
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set contentChangedBy
     *
     * @param string $contentChangedBy
     *
     * @return Variants
     */
    public function setContentChangedBy($contentChangedBy)
    {
        $this->contentChangedBy = $contentChangedBy;

        return $this;
    }

    /**
     * Get contentChangedBy
     *
     * @return string
     */
    public function getContentChangedBy()
    {
        return $this->contentChangedBy;
    }
}
