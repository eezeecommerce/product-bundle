<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use eezeecommerce\CartBundle\Core\AbstractProductExtrasEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="options")
 * @ORM\Entity(repositoryClass="eezeecommerce\ProductBundle\Entity\OptionsRepository")
 */
class Options extends AbstractProductExtrasEntity implements Translatable
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotNull
     * @ORM\Column(type="string", columnDefinition="ENUM('file', 'text', 'array', 'textarea', 'other')")
     */
    protected $type;

    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=50)
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @Gedmo\Locale
     */
    protected $locale;
    
    /**
     * @Assert\NotNull
     * @Assert\Type("boolean")
     * @ORM\Column(type="boolean")
     */
    protected $optional;
    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $stock_code;
    /**
     * @Assert\Type("string")
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $bay_number;
    /**
     * @Assert\NotBlank
     * @ORM\Column(type="decimal", precision=19, scale=4)
     */
    protected $base_price;
    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="\eezeecommerce\ProductBundle\Entity\OptionChoices", mappedBy="option", cascade={"persist"})
     */
    protected $option_choices;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $placeholder;
    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="eezeecommerce\OrderBundle\Entity\OptionsOrderlines", mappedBy="options")
     */
    protected $orderlines;
    /**
     * @Assert\Type("int")
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $max_length;
    /**
     * @Assert\Valid
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\UploadBundle\Entity\Files", cascade={"persist"})
     * @ORM\JoinTable(name="option_files",
     * joinColumns={@ORM\JoinColumn(name="option_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")})
     */
    private $image;
    /**
     * @Assert\Valid
     * @ORM\ManyToMany(targetEntity="Options", inversedBy="master", cascade={"persist"})
     * @ORM\JoinTable(name="option_children",
     *     joinColumns={@ORM\JoinColumn(name="slave_option_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="master_option_id", referencedColumnName="id")}
     * )
     */
    private $slaves;
    /**
     * @Assert\Valid
     * @ORM\ManyToMany(targetEntity="Options", mappedBy="slaves", cascade={"persist"})
     */
    private $master;
    
    /**
     * @var string $createdBy
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\Column(type="string")
     */
    private $createdBy;

    /**
     * @var string $updatedBy
     *
     * @Gedmo\Blameable(on="update")
     * @ORM\Column(type="string")
     */
    private $updatedBy;

    /**
     * @var string $contentChangedBy
     *
     * @ORM\Column(name="content_changed_by", type="string", nullable=true)
     * @Gedmo\Blameable(on="change", field={"type", "name", "description", "optional", "stock_code", "bay_number", "base_price", "max_length", "option_choices"})
     */
    private $contentChangedBy;


    /**
     * Options constructor.
     */
    public function __construct()
    {
        $this->image = new ArrayCollection();
        $this->option_choices = new ArrayCollection();
        $this->slaves = new ArrayCollection();
        $this->orderlines = new ArrayCollection();
        $this->master = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Options
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set product
     *
     * @param integer $product
     *
     * @return Options
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return integer
     */
    public function getProduct()
    {
        return $this->product;
    }

    public function getNameCode()
    {
        return $this->name." - ".$this->stock_code;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Options
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Options
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Options
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get image
     *
     * @return integer
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param integer $image
     *
     * @return Options
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get stockCode
     *
     * @return string
     */
    public function getStockCode()
    {
        return $this->stock_code;
    }

    /**
     * Set stockCode
     *
     * @param string $stockCode
     *
     * @return Options
     */
    public function setStockCode($stockCode)
    {
        $this->stock_code = $stockCode;

        return $this;
    }

    /**
     * Get bayNumber
     *
     * @return string
     */
    public function getBayNumber()
    {
        return $this->bay_number;
    }

    /**
     * Set bayNumber
     *
     * @param string $bayNumber
     *
     * @return Options
     */
    public function setBayNumber($bayNumber)
    {
        $this->bay_number = $bayNumber;

        return $this;
    }

    /**
     * Add image
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $image
     *
     * @return Options
     */
    public function addImage(\eezeecommerce\UploadBundle\Entity\Files $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $image
     */
    public function removeImage(\eezeecommerce\UploadBundle\Entity\Files $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get optional
     *
     * @return boolean
     */
    public function getOptional()
    {
        return $this->optional;
    }

    /**
     * Set optional
     *
     * @param boolean $optional
     *
     * @return Options
     */
    public function setOptional($optional)
    {
        $this->optional = $optional;

        return $this;
    }

    /**
     * Get basePrice
     *
     * @return string
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * Set basePrice
     *
     * @param string $basePrice
     *
     * @return Options
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;

        return $this;
    }

    /**
     * Add optionChoice
     *
     * @param \eezeecommerce\ProductBundle\Entity\OptionChoices $optionChoice
     *
     * @return Options
     */
    public function addOptionChoice(\eezeecommerce\ProductBundle\Entity\OptionChoices $optionChoice)
    {
        $this->option_choices[] = $optionChoice;
        
        $optionChoice->setOption($this);

        return $this;
    }

    /**
     * Remove optionChoice
     *
     * @param \eezeecommerce\ProductBundle\Entity\OptionChoices $optionChoice
     */
    public function removeOptionChoice(\eezeecommerce\ProductBundle\Entity\OptionChoices $optionChoice)
    {
        $this->option_choices->removeElement($optionChoice);
    }

    /**
     * Get optionChoices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionChoices()
    {
        return $this->option_choices;
    }

    /**
     * Add orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OptionsOrderlines $orderline
     *
     * @return Options
     */
    public function addOrderline(\eezeecommerce\OrderBundle\Entity\OptionsOrderlines $orderline)
    {
        $this->orderlines[] = $orderline;

        return $this;
    }

    /**
     * Remove orderline
     *
     * @param \eezeecommerce\OrderBundle\Entity\OptionsOrderlines $orderline
     */
    public function removeOrderline(\eezeecommerce\OrderBundle\Entity\OptionsOrderlines $orderline)
    {
        $this->orderlines->removeElement($orderline);
    }

    /**
     * Get orderlines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderlines()
    {
        return $this->orderlines;
    }

    /**
     * Get maxLength
     *
     * @return integer
     */
    public function getMaxLength()
    {
        return $this->max_length;
    }

    /**
     * Set maxLength
     *
     * @param integer $maxLength
     *
     * @return Options
     */
    public function setMaxLength($maxLength)
    {
        $this->max_length = $maxLength;

        return $this;
    }

    /**
     * Add slave
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $slave
     *
     * @return Options
     */
    public function addSlave(\eezeecommerce\ProductBundle\Entity\Options $slave)
    {
        $this->slaves[] = $slave;

        return $this;
    }

    /**
     * Remove slave
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $slave
     */
    public function removeSlave(\eezeecommerce\ProductBundle\Entity\Options $slave)
    {
        $this->slaves->removeElement($slave);
    }

    /**
     * Get slaves
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlaves()
    {
        return $this->slaves;
    }

    /**
     * Add master
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $master
     *
     * @return Options
     */
    public function addMaster(\eezeecommerce\ProductBundle\Entity\Options $master)
    {
        $this->master[] = $master;

        return $this;
    }

    /**
     * Remove master
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $master
     */
    public function removeMaster(\eezeecommerce\ProductBundle\Entity\Options $master)
    {
        $this->master->removeElement($master);
    }

    /**
     * Get master
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaster()
    {
        return $this->master;
    }

    /**
     * Get placeholder
     *
     * @return string
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * Set placeholder
     *
     * @param string $placeholder
     *
     * @return Options
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     *
     * @return Options
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     *
     * @return Options
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set contentChangedBy
     *
     * @param string $contentChangedBy
     *
     * @return Options
     */
    public function setContentChangedBy($contentChangedBy)
    {
        $this->contentChangedBy = $contentChangedBy;

        return $this;
    }

    /**
     * Get contentChangedBy
     *
     * @return string
     */
    public function getContentChangedBy()
    {
        return $this->contentChangedBy;
    }
}
